var mongoose = require('mongoose');

var historialSchema = mongoose.Schema({
		job: String,
		username: String,
		d_started: String,
		d_completed: String,
		category: String,
		h_worked: String,
		job_success: String
			});

module.exports = mongoose.model('Historial', historialSchema);
