var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	
		username: String,
		password: String,
		nombre: String,
		apellido: String,
		email: String,
		admin_priv: String,
		titulo: String,
		skill: String,
		pais: String,
		ciudad: String,
		utc: String,
		jobs: String,
		h_trab: String,
		reviews: String,
		clients: String,
		earnigs: String,
		description: String,
		type: String,
		fecha: Date,
		url: String,
		verifi: String,
		comname: String
	
});

module.exports = mongoose.model('User', userSchema);