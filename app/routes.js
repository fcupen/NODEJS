var User = require('./models/user');
var Historial = require('./models/historial');
var Joboffer = require('./models/joboffer');
var Addons = require('./models/addons');
//var Profile = require('./models/profile');
module.exports = function(app, passport){

	app.get('/', function(req, res){
		res.render('index.ejs');
	});

	app.get('/login', function(req, res){
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	app.post('/login', passport.authenticate('local-login', {
		successRedirect: '/pf',
		failureRedirect: '/login',
		failureFlash: true
	}));

	app.post('/cf', function(req,res){
		var data = {
			nombre:req.body.nombre,
			apellido:req.body.apellido,
			email:req.body.email,
			username:req.body.username,
			comname:req.body.comname,
			password:req.body.password,
			pais:req.body.pais,
		    admin_priv : "f",
		    titulo : "Developer",
		    skill : "PHP, AngularJS, MySQL",
		    pais : "Venezuela",
		    ciudad : "Valencia",
		    utc : "-4:30",
		    jobs : "4",
		    h_trab : "100",
		    reviews : "8",
		    clients : "8",
		    earnigs : "2500",
		    description : "Hola a todos",
		    url : "expert.com/"+req.body.email,
		    verifi : "True"
		}
		var user = new User(data);

		user.save(function(err){
			console.log(user);
			res.render('index.ejs');
		})
	});







	app.post('/cc', function(req,res){
		var data = {
			nombre:req.body.nombre,
			apellido:req.body.apellido,
			email:req.body.email,
			username:req.body.email,
			comname:req.body.comname,
			password:req.body.password,
		    admin_priv : "c",
		    titulo : "Developer",
		    skill : "PHP, AngularJS, MySQL",
		    pais : "Venezuela",
		    ciudad : "Valencia",
		    utc : "-4:30",
		    jobs : "4",
		    h_trab : "100",
		    reviews : "8",
		    clients : "8",
		    earnigs : "2500",
		    description : "Hola a todos",
		    url : "expert.com/"+req.body.email,
		    verifi : "True"
		}
		var user = new User(data);

		user.save(function(err){
			console.log(user);
			res.render('index.ejs');
		})
	});

	app.post('/cc', passport.authenticate('local-signup', {
		successRedirect: '/',
		failureRedirect: '/cc',
		failureFlash: true
	}));

	app.post('/cf', passport.authenticate('local-signup', {
		successRedirect: '/',
		failureRedirect: '/cf',
		failureFlash: true
	}));

	app.get('/cf', function(req, res){
		res.render('cf.ejs', { user: req.user });
	});

	app.get('/cc', function(req, res){
		res.render('cc.ejs', { user: req.user });
	});

	app.get('/search', isLoggedIn, function(req, res){
		res.render('search.ejs', { user: req.user });
	});

	app.get('/pc', isLoggedIn, function(req, res){
		Historial.find({"username":req.user.username}, function(err,documento){
		Joboffer.find({"username":req.user.username}, function(err2,documento2){
		Addons.find({"username":req.user.username}, function(err3,documento3){
		res.render('pc.ejs', { user: req.user , historial: documento, joboffers: documento2, addons: documento3});
		});});});
	});

	app.get('/pf', isLoggedIn, function(req, res){
		console.log(req.user.admin_priv);
		if(req.user.admin_priv == "c"){
		res.render('inic.ejs', { user: req.user});
		}else{
		Historial.find({"username":req.user.username}, function(err,documento){
		Joboffer.find({"username":req.user.username}, function(err2,documento2){
		Addons.find({"username":req.user.username}, function(err3,documento3){
		res.render('pf.ejs', { user: req.user , historial: documento, joboffers: documento2, addons: documento3});
		});});});}});

	app.get('/logout', function(req, res){
		req.logout();
		res.redirect('/');
	})
};

function isLoggedIn(req, res, next) {
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('/login');
}